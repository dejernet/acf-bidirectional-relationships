<?php
/*
 * Plugin Name: ACF Bidirectional Relationships
 * Plugin URI: https://www.advancedcustomfields.com/
 * Description: Advanced Custom Fields Addon that adds the ability to map relationship fields between posts
 * Version: 0.2.0
 * Author: DJ Nolder
 * Author URI: http://www.dejer.net/
 * Bitbucket Plugin URI: dejernet/acf-bidirectional-relationships
 */


/**
 * FUTURE VERSION
 * 
 * In the future, mappings will be based on acf-field-group and acf-field IDs, rather than post_type and field_name
 * 
 */
if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

new acf_bidirectional_relationships();

/**
 * acf_bidirectional_relationships is a wrapper class used for the ACF Bidirectional Relationships plugin
 */
class acf_bidirectional_relationships {

	/**
 	* @var array $mappings - array of arrays, internal arrays represent bidirectional associations formatted post_type:acf_field_name
 	*/ 
	private $mappings = array(
		array('location:dr_in_charge', 'staff:staff_location'),
		array('location:staff_physician_assistants', 'staff:staff_location'),
		array('location:staff_aestheticians', 'staff:staff_location'),
		array('location:promotions', 'promotions:location'),
		array('treatments:staff', 'staff:treatments'),
	);

	function __construct() {
		add_action( 'admin_init', array( $this, 'check_acf_active' ) );

		add_filter('acf/update_value', array( $this, 'check_for_bidirectional_mapping' ), 10, 3);
	}

	/**
	 * Action for admin_init that makes sure the plugin Advanced Custom Fields Pro is installed and active before allowing this plugin to be activated.
	 * 
	 * @return void
	 */
	function check_acf_active() {
		if ( is_admin() && current_user_can( 'activate_plugins' ) &&  !is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ) {
			add_action( 'admin_notices', function() {
				echo '<div class="error"><p>The "ACF Bidirectional Relationships" plugin requires the "Advanced Custom Fields Pro" plugin to be installed and active.</p></div>';
			});

			deactivate_plugins( plugin_basename( __FILE__ ) ); 

			if ( isset( $_GET['activate'] ) ) {
				unset( $_GET['activate'] );
			}
		}
	}

	/**
	 * Filter for acf/update_value that watches an array of mappings for $field to update the corresponding field
	 * 
	 * @param mixed $value The value to update.
	 * @param string $post_id The post ID for this value.
	 * @param array $field The field array.
	 * @return type
	 */
	function check_for_bidirectional_mapping($value, $post_id, $field) {
		// bail early if this filter was triggered from the update_field() function called within the loop below
		// - this prevents an inifinte loop
		if( !empty($GLOBALS[ 'is_updating_' . $field['name'] ]) ) return $value;

		$maps = array();

		// process the stored maps and build a mapping array
		foreach ($this->mappings as $mapping) {
			$maps[$mapping[0]][] = $mapping[1];
			$maps[$mapping[1]][] = $mapping[0];
		}

		// get current post type by id
		$map_from = get_post_type($post_id) . ':' . $field['name'];

		$map_tos = $maps[$map_from];

		if ($map_tos) {

			foreach ($map_tos as $map_to) {
				list($map_to_type, $map_to_field) = explode(':', $map_to);

				// verify taxonomy matches if it exists
				if ( !$this->verify_taxonomy($post_id, $map_to_field) ) continue;

				// set global variable to avoid inifite loop
				$GLOBALS[ 'is_updating_' . $map_to_field ] = 1;

				if ( is_array($value) ) {
					foreach( $value as $post_id2 ) {
						// verify post type = map_to_type
						if ( get_post_type($post_id2) == $map_to_type ) {

							$value2 = get_field($map_to_field, $post_id2, false);
							
							// allow for selected posts to not contain a value
							if ( empty($value2) ) $value2 = array();
							
							// bail early if the current $post_id is already found in selected post's $value2
							if ( in_array($post_id, $value2) ) continue;

							// append the current $post_id to the selected post's value
							$value2[] = $post_id;
					
							// update the selected post's value
							update_field($map_to_field, $value2, $post_id2);

							// log the action
							$this->write_log('ACF BIDIRECTIONAL: Mapped post_id ' . $post_id . ' to ' . $map_to_field . " for post_id " . $post_id2);
						}
					}
				}

				// find posts which have been removed
				$old_value = get_field($field['name'], $post_id, false);
				
				if( is_array($old_value) ) {
					foreach( $old_value as $post_id2 ) {
				
						// bail early if this value has not been removed
						if( is_array($value) && in_array($post_id2, $value) ) continue;
				
						// load existing related posts
						$value2 = get_field($map_to_field, $post_id2, false);
				
						// bail early if no value
						if( empty($value2) ) continue;
				
						// find the position of $post_id within $value2 so we can remove it
						$pos = array_search($post_id, $value2);
				
						// remove
						unset( $value2[$pos] );

						// update the un-selected post's value (use field's key for performance)
						update_field($map_to_field, $value2, $post_id2);

						// log the action
						$this->write_log('ACF BIDIRECTIONAL: Removed post_id ' . $post_id . ' from ' . $map_to_field . " for post_id " . $post_id2);
					}
				}

				// reset global variable to allow this filter to function as per normal
				$GLOBALS[ 'is_updating_' . $map_to_field ] = 0;
			}

		}
		return $value;
	}

	/**
	 * Verifies taxonomay exists for $map_to_field and exists for $post_id
	 * @param int $post_id 
	 * @param string $map_to_field 
	 * @return bool
	 */
	private function verify_taxonomy($post_id, $map_to_field) {
		$map_to_acf_field = acf_get_field($map_to_field);

		// if there is no taxonomy, then return as verified
		if ( !$map_to_acf_field['taxonomy'] ) {
			return true;
		}

		// go through the field taxonomies and see if the post_id matches
		foreach ( $map_to_acf_field['taxonomy'] as $taxonomy ) {

			// parse the taxonomy details
			list($tax_name, $tax_value) = explode(':', $taxonomy);

			// does the origional post have the correct taxonomy?
			if ( has_term( $tax_value, $tax_name, $post_id ) ) {
				return true;
			}

		}

		// if we make it this far, taxonomy verification has failed
		return false;
	}

	/**
	 * Parses and writes log information to php's error_log
	 * @param mixed $log 
	 * @return void
	 */
	private function write_log ( $log )  {
		if ( is_array( $log ) || is_object( $log ) ) {
			error_log( print_r( $log, true ) );
		} else {
			error_log( $log );
		}
	}


}

class acf_bidirectional_map {

}